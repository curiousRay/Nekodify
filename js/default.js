function setColor(bodyPart, destColor){
  if (bodyPart == 'head') $(".s-head").find("path").attr("fill", destColor);
  if (bodyPart == 'body') $(".s-body").find("path").attr("fill", destColor);
  if (bodyPart == 'mouth') $(".s-mouth").attr("stroke", destColor);
  if (bodyPart == 'eye') $(".s-eye").find("path").attr("fill", destColor);
  if (bodyPart == 'nose') $(".s-nose").find("path").attr("fill", destColor);
  if (bodyPart == 'ear') $(".s-ear").find("path").attr("fill", destColor);
  if (bodyPart == 'earinside') $(".s-earinside").find("path").attr("fill", destColor);
  if (bodyPart == 'belly') $(".s-belly").find("path").attr("fill", destColor);
  if (bodyPart == 'back') $(".s-back").find("path").attr("fill", destColor);
  if (bodyPart == 'leg') $(".s-leg").find("path").attr("fill", destColor);
  if (bodyPart == 'foot') $(".s-foot").find("path").attr("fill", destColor);
  if (bodyPart == 'tail') $(".s-tail").attr("stroke", destColor);
  if (bodyPart == 'tailcap') $(".s-tailcap").find("path").attr("fill", destColor);
  if (bodyPart == 'cap') $(".s-cap").find("path").attr("fill", destColor);
  if (bodyPart == 'collar') $(".s-collar").find("path").attr("fill", destColor);
  if (bodyPart == 'bowtie') $(".s-bowtie").find("path").attr("fill", destColor);
}

function setBgColor(destColor){
  $(".bg").css("fill", destColor);
}

function reset(){
  $(".s-head").find("path").attr("fill", 'black');
  $(".s-body").find("path").attr("fill", 'black');
  $(".s-mouth").attr("stroke", 'black');
  $(".s-eye").find("path").attr("fill", 'black');
  $(".s-nose").find("path").attr("fill", 'black');
  $(".s-ear").find("path").attr("fill", 'black');
  $(".s-earinside").find("path").attr("fill", 'black');
  $(".s-belly").find("path").attr("fill", 'black');
  $(".s-back").find("path").attr("fill", 'black');
  $(".s-leg").find("path").attr("fill", 'black');
  $(".s-foot").find("path").attr("fill", 'black');
  $(".s-tail").attr("stroke", 'black');
  $(".s-tailcap").find("path").attr("fill", 'black');
  $(".s-cap").find("path").attr("fill", 'black');
  $(".s-collar").find("path").attr("fill", 'black');
  $(".s-bowtie").find("path").attr("fill", 'black');
  setBgColor("white");
}

function getCurrBodyPart(){
  return ($(".active").attr('id'));
}

function download(){
  var canvas = document.querySelector("canvas");
  context = canvas.getContext("2d");
  var image = new Image;

  //将浏览器中的svg改动复制到blob

  //拼接svg文档
  var htmlParts = [
    "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 48 48\" style=\"enable-background:new 0 0 48 48;\" xml:space=\"preserve\">"
    +document.getElementById("model").innerHTML
    +"</svg>"
  ];

  var myBlob = new Blob(htmlParts, {"type" : "image/svg+xml" });
  image.src =  window.URL.createObjectURL(myBlob);
  image.onload = function() {
    context.drawImage(image, 0, 0);
    var a = $("#buttonGroup").find("a")[0];//绑定下载按钮
    a.download = "MyNeko.png";
    a.href = canvas.toDataURL("image/png");
    a.click();//立即下载
  }
}

$(document).ready(function(){
  reset();
});